﻿using System;
using System.Text;

namespace LR1_uravnenie
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Решаем уравнение 2-й степени\n ax2 + bx + c = 0\n");
            Console.WriteLine("Введите a: ");
            double a = GetValue();
            Console.WriteLine("Введите b: ");
            double b = GetValue();  
            Console.WriteLine("Введите c: ");
            double c = GetValue();

            float d = (float)(Math.Pow(b, 2) - (4 * a * c));
            if(d>0)
            {
                Console.WriteLine("D>0 существует 2 корня");
                float x1 = (float)((-b + Math.Sqrt(d)) / (2 * a));
                float x2 = (float)((b + Math.Sqrt(d)) / (2 * a));
                Console.WriteLine("X1 = {0} , X2 = {1}", x1, x2);
            }
            else if(d==0)
            {
                Console.WriteLine("D=0 существует 1 корень");
                float x1 = (float)((-b) / (2 * a));
                Console.WriteLine("X1 = ", x1);
            }else
            {
                Console.WriteLine("D>0 делаем вывод о том, что корней на множестве действительных чисел нет");
            }
        }
        static double GetValue()
        {
            StringBuilder sb = new StringBuilder(4);
            int curStart = Console.CursorLeft;
            int curOffset = 0;
            ConsoleKeyInfo keyInfo;
            do
            {
                keyInfo = Console.ReadKey(true);
                if (char.IsDigit(keyInfo.KeyChar) && sb.Length < 4)
                {
                    sb.Insert(curOffset, keyInfo.KeyChar);
                    curOffset++;
                    Console.Write(keyInfo.KeyChar);
                }
                if (keyInfo.Key == (ConsoleKey)37 && curOffset > 0) curOffset--;
                if (keyInfo.Key == ConsoleKey.RightArrow && curOffset < sb.Length) curOffset++;
                if (keyInfo.Key == ConsoleKey.Backspace && curOffset > 0)
                {
                    curOffset--;
                    sb.Remove(curOffset, 1);
                    Console.CursorLeft = curStart;
                    Console.Write(sb.ToString().PadRight(4));
                }
                if (keyInfo.Key == ConsoleKey.Delete && curOffset < sb.Length)
                {
                    sb.Remove(curOffset, 1);
                    Console.CursorLeft = curStart;
                    Console.Write(sb.ToString().PadRight(4));
                }
                Console.CursorLeft = curStart + curOffset;
            }
            while (!(keyInfo.Key == ConsoleKey.Enter && sb.Length > 0));
            Console.WriteLine();
            return double.Parse(sb.ToString());
        }
    }
}
