﻿using System;
using System.Text;
//Дана строка, слова которой разделены пробелами.Распознать в ней слова,
//являющиеся числами в шестнадцатеричной системе счисления, и вывести их
//десятичный эквивалент.
namespace LR2_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] str = Console.ReadLine().Split(' ');

            int[] array = new int[100];
            for (int j = 0; j < str.Length; j++)
            {
                try
                {
                    array [j] = Convert.ToInt32(str[j], 16);
                    Console.WriteLine(array[j]);
                }catch(Exception ex)
                {
                    Console.WriteLine(ex);
                    continue;
                }
            }
           
        }
    }
}
