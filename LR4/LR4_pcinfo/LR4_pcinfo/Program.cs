﻿using System;
using infoAboutPC;
namespace LR4_pcinfo
{
    class Program
    {
        //[DllImport("MyLibrary.dll")]
        //public static extern IntPtr OutputResult(string str, List<string> list);
        static void Main(string[] args)
        {
            //OutputResult("Процессор:", MyLibrary.GetHardwareInfo("Win32_Processor", "Name"));
            MyLibrary.OutputResult("Процессор:", MyLibrary.GetHardwareInfo("Win32_Processor", "Name"));
            MyLibrary.OutputResult("Производитель:", MyLibrary.GetHardwareInfo("Win32_Processor", "Manufacturer"));
            MyLibrary.OutputResult("Описание:", MyLibrary.GetHardwareInfo("Win32_Processor", "Description"));
            MyLibrary.OutputResult("Видеокарта:", MyLibrary.GetHardwareInfo("Win32_VideoController", "Name"));
            MyLibrary.OutputResult("Видеопроцессор:", MyLibrary.GetHardwareInfo("Win32_VideoController", "VideoProcessor"));
            MyLibrary.OutputResult("Версия драйвера:", MyLibrary.GetHardwareInfo("Win32_VideoController", "DriverVersion"));
            MyLibrary.OutputResult("Объем памяти (в байтах):", MyLibrary.GetHardwareInfo("Win32_VideoController", "AdapterRAM"));
            MyLibrary.OutputResult("Название дисковода:", MyLibrary.GetHardwareInfo("Win32_CDROMDrive", "Name"));
            MyLibrary.OutputResult("Жесткий диск:", MyLibrary.GetHardwareInfo("Win32_DiskDrive", "Caption"));
            MyLibrary.OutputResult("Объем (в байтах):", MyLibrary.GetHardwareInfo("Win32_DiskDrive", "Size"));
        }
    }
}
