﻿using AForge.Imaging;
using AForge.Imaging.Filters;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows;

namespace LR4_print
{
    sealed class Program
    {
        [DllImport("user32.dll", EntryPoint = "GetDC")]
        static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport("gdi32.dll")]
        static extern bool FillRgn(IntPtr hdc, IntPtr hrgn, IntPtr hbr);

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateRectRgn(int nLeftRect, int nTopRect, int nRightRect,
            int nBottomRect);

        [DllImport("gdi32.dll")]
        static extern IntPtr CreateSolidBrush(uint crColor);

        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out POINT lpPoint);

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public static implicit operator Point(POINT point)
            {
                return new Point(point.X, point.Y);
            }
        }
        static void Main(string[] args)
        {

            IntPtr wDc = GetDC(IntPtr.Zero);
            IntPtr brush = CreateSolidBrush((uint)ColorTranslator.ToWin32(Color.Red));
            POINT lpPoint;

            while (true)
            {
                GetCursorPos(out lpPoint);
                FillRgn(wDc, CreateRectRgn(lpPoint.X, lpPoint.Y, lpPoint.X +1, lpPoint.Y+1), brush);
            }

        }
    }
}
