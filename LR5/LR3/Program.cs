﻿#nullable enable
using System;
using System.Text.RegularExpressions;

namespace LR3
{
    enum TypeOfSport
    {
        Badminton = 0,
        Basketball = 1,
        Baseball,
        Boxing,
        Cycling,
        Handball,
        Judo
    };
    class Human
    {
        public static int NumberOfPeople = 0;

        protected struct GeneralInfo
        {
            static string _FirstName = " ";
            static string _LastName = " ";
            static string _MiddleName;

            static internal string FirstName
            {
                get => _FirstName; 
                set
                {
                    if (Regex.Match(value, "[а-яА-Я]").Success)
                    {
                        _FirstName = value;
                    }
                }
            }
            static internal string LastName
            {
                get => _LastName;
                set
                {
                    if (Regex.Match(value, "[а-яА-Я]").Success)
                    {
                        _LastName = value;
                    }
                }
            }
            static internal string MiddleName
            {
                get => _MiddleName;
                set
                {
                    if (Regex.Match(value, "[а-яА-Я]").Success)
                    {
                        _MiddleName = value;
                    }
                }
            }
        }
        protected int age = default;

        public Human(string first, string last, string middle, int old) =>
            (GeneralInfo.FirstName, GeneralInfo.LastName, GeneralInfo.MiddleName, age, NumberOfPeople)
            = (first, last, middle, old, ++NumberOfPeople);
        public string this[string propname]
        {
            get
            {
                switch (propname)
                {
                    case "name": return GeneralInfo.FirstName;
                    case "lstname": return GeneralInfo.LastName;
                    default: return "не найдено";
                }
            }
            set
            {
                switch (propname)
                {
                    case "name":
                        GeneralInfo.FirstName = value;
                        break;
                    case "lstname":
                        GeneralInfo.LastName = value;
                        break;
                    default:
                        break;
                }
            }
        }
        public virtual string Print() => $" {GeneralInfo.FirstName} {GeneralInfo.LastName} {GeneralInfo.MiddleName}";
    }
    class Sportsmen : Human
    {
        public string TypeOfSport { get; set; }

        public Sportsmen(string FirstName, string LastName, string MiddleName, int age, string typeofsport)
            : base(FirstName, LastName, MiddleName, age)
        {
            (TypeOfSport) = (typeofsport);
        }
        public Sportsmen(string FirstName, string LastName, string MiddleName, int age, int typeofsport)
           : base(FirstName, LastName, MiddleName, age)
        {
            (TypeOfSport) = Enum.GetName(typeof(TypeOfSport), typeofsport);
        }

    }
    class Specialist : Sportsmen
    {
        public Specialist(string proff, string FirstName, string LastName, string MiddleName, int age, string typeofsport) :
            base(FirstName, LastName, MiddleName, age, typeofsport) => (vocation) = (proff);

        public Specialist(string proff, string FirstName, string LastName, string MiddleName, int age, int typeofsport) :
            base(FirstName, LastName, MiddleName, age, typeofsport) => (vocation) = (proff);

        public string vocation;

        public void AddToPrint(string str1)
        {
            Console.WriteLine(Print() + " " + str1);
        }
        public void AddToPrint(string str1, string str2)
        {
            Console.WriteLine(Print() + " " + str1 + " " + str2);
        }
        public void AddToPrint(params string[] args)
        {
            Console.WriteLine(Print());
            foreach (var x in args)
            {
                Console.WriteLine(x);
            }
        }
        public override string Print() => $" {GeneralInfo.FirstName} {GeneralInfo.LastName} {GeneralInfo.MiddleName} {TypeOfSport} {vocation}";
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Human.NumberOfPeople);
            Human test = new Human(first: "Владислав", last: "Аринович", old: 18, middle: "Игоревич");
            Specialist test2 = new Specialist("Тренер", "Владислав", "Аринович", "Игоревич", 25, Enum.GetName(typeof(TypeOfSport), 3));
            Specialist katya = new Specialist("Психолог", "Катя", "Пульмановская", "как-то", 25, 0);
            Console.WriteLine(katya.Print());
            Console.WriteLine(test2["name"]);
            test2.AddToPrint("норм");
            test2.AddToPrint("норм", "или нет?");
            Console.WriteLine(Human.NumberOfPeople);
        }
    }
}